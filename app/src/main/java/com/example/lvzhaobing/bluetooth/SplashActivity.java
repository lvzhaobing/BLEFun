package com.example.lvzhaobing.bluetooth;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SplashActivity extends AppCompatActivity {
    private Handler handler = new Handler();
   private Runnable runnable;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //判断第一次启动
        Boolean isFirstIn = false;
        SharedPreferences pref = this.getSharedPreferences("myActivityName", 0);
        //取得相应的值，如果没有该值，说明还未写入，用true作为默认值
        isFirstIn = pref.getBoolean("isFirstIn", true);

        if(!isFirstIn){
            Intent intent = new Intent(SplashActivity.this , MainActivity.class);
            //启动
            startActivity(intent);
            finish();
        }
        else{
            setContentView(R.layout.activity_splash);
            //定时启动
            handler.postDelayed(runnable=new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(SplashActivity.this , MainActivity.class);
                    //启动
                    startActivity(intent);
                    finish();
                }
            },3000);
        }
        //保存第一次启动
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("isFirstIn", false);
        editor.commit();

        //声明一个按钮，点击启动
        Button startButton=(Button)findViewById(R.id.startbutton);
        //为该按钮设置监听事件
        startButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(SplashActivity.this , MainActivity.class);
                //启动
                startActivity(intent);
                finish();
                if(runnable!=null){
                handler.removeCallbacks(runnable);
                }
            }
            });

    }

}
